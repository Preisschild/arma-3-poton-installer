# notes
#
# 1) find out if Arma 1.82 or 1.90 is installed
# 2) detect OS and distro
# 3) install needed tools
# 4) make sure a version of proton is installed
# 5) use either native2proton method or custom solution
# 7) install needed libraries for proton use
# 8) move imagehlp.dll from install folder to proton folder
# 9) add imagehlp, api-win-crt-private to winecfg
# 10) cleanup
#


import os, platform, subprocess, re, tarfile
def main():
    print ("Setting up...")


    def find_libraries():
        home = os.path.expanduser("~")
        if os.path.isfile(home+"/.steam/steam/steamapps/libraryfolders.vdf"):
            steam_library = str(home+"/.steam/steam/steamapps/libraryfolders.vdf")
            steamapps_dir = str(home+"/.steam/steam/steamapps")
        elif os.path.isfile(home+"/.local/share/Steam/steamapps/libraryfolders.vdf"):
            steam_library = str(home+"/.local/share/Steam/steamapps/libraryfolders.vdf")
            steamapps_dir = str(home+"/.local/share/Steam/steamapps")
        
        #if steam_library != None:
        #
        #    for line in open(steam_library, 'r'):
        #        stripline = line.lstrip()
        #        if stripline[1].isdigit():
        #            if os.path.isdir(re.findall(r'"([^"]*)"', line)[1]+"/steamapps/common"):
        #                libraries.append(re.findall(r'"([^"]*)"', line)[1]+"/steamapps/common")
        #    if not libraries:
        #        libraries.append(steamapps_dir+"/common")
        return steamapps_dir, steam_library    

    def detect(distro):
        linuxVer = platform.release()
        linuxVer = linuxVer.lower()

        if platform.system() == "Linux":
            if distro.lower() in linuxVer:
                usrInput = input(distro+" was detected, is this correct?: (Y/N)\n")
                if usrInput.lower() == "y":
                    print ("Installing and setting up Arma 3 Proton...")
                    return True
                else:
                    print ("Exiting installer...")
                    exit(1)
            else:
                return False
        else:
            print (platform.system()+" is not supported by this installer, exiting...")
            exit

    def install(distro, base_dir, steamapps_dir):

        def rename_exes(base_dir, steamapps_dir):
            print ("Renaming Arma 3 exe's...")
            arma3_dir = str(steamapps_dir+"/common/Arma 3")
            if os.path.isfile(arma3_dir+"/arma3launcher.exe") and os.path.isfile(arma3_dir+"/arma3_x64.exe"):
                os.system("mv "+steamapps_dir+"/common/Arma\ 3"+"/arma3launcher.exe "+steamapps_dir+"/common/Arma\ 3"+"/arma3launcher.exe.backup")
                os.system("mv "+steamapps_dir+"/common/Arma\ 3"+"/arma3_x64.exe "+steamapps_dir+"/common/Arma\ 3"+"/arma3launcher.exe")
            else:
                print ("Warning: arma3launcher.exe or arma3_x64.exe not found, did you already rename it?")

        print ("Installing...")
        if distro == "Debain" or distro == "Ubuntu":
            if os.system("sudo apt install wine winetricks mesa-vulkan-drivers vulkan-utils") != 0:
                print ("An error occurred, Aborting install...")
                exit
            setup_prefix(base_dir, steamapps_dir)
            rename_exes(base_dir, steamapps_dir)

            print ("Install complete! enjoy your game.")
            exit(0)

        elif distro == "Arch":
            setup_prefix(base_dir, steamapps_dir)
            rename_exes(base_dir, steamapps_dir)

            print ("Install complete! you used arch btw.")
            exit(0)

        elif distro == "Fedora":
            setup_prefix(base_dir, steamapps_dir)
            rename_exes(base_dir, steamapps_dir)

            print ("Install complete! *tips fedora*.")
            exit(0)

    def setup_prefix(base_dir, steamapps_dir):
        if not os.path.isdir(base_dir+"/Resources/winetricks"):
            print ("Downloading winetricks...")
            os.mkdir(base_dir+"/Resources/winetricks")
            os.system('wget -P '+base_dir+'/Resources/winetricks/ "https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks"')
            os.chmod(base_dir+'/Resources/winetricks/winetricks', 0o775)
            
        print ("Setting up prefix...")
        if not os.path.isdir(steamapps_dir+"/common/Proton 4.2"):
            usrInput = input("Proton 4.2 does not appear to be installed, Install now? (Y/N)\n")
            if usrInput.lower() == "y":
                install_proton(base_dir, steamapps_dir)
            else:
                print ("Aborting install...")
                exit(1)
        proton_dir = str(steamapps_dir+"/common/Proton\\ 4.2")
        
        while not os.path.isfile(steamapps_dir+"/compatdata/107410/pfx/user.reg"):
                input("The prefix has not yet been initialized, please click 'play' in steam and start Arma 3 to perform first time setup, you might get errors but you can ignore them\n")


        os.system("cp "+base_dir+"/Resources/imagehlp.dll.so "+proton_dir+"/dist/lib64/wine/imagehlp.dll.so")
        
        os.environ["WINEDLLPATH"] = str(proton_dir+"/dist/lib64/wine:"+proton_dir+"/dist/lib/wine")
        os.environ["PATH"] = str(proton_dir+"/dist/bin/:"+proton_dir+"/dist/lib/:"+proton_dir+"/dist/lib64/:/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin:/snap/bin")
        os.environ["WINEPREFIX"] = str(steamapps_dir+"/compatdata/107410/pfx/")

        winetricks = str(base_dir+"/Resources/winetricks/winetricks")
        subprocess.call([winetricks, "d3dcompiler_43", "d3dx10_43", "d3dx11_43", "xact_x64"])
        os.system("sleep 3") # give winetricks some time to calm down

        # set override dlls
        # refuses to work if the extra indent isn't there, no idea why
        def override_dll(dllName, loadType, insertPoint):
                with open(steamapps_dir+"/compatdata/107410/pfx/user.reg", 'r+') as f:
                    text = f.read()
                    if not dllName in text:
                        with open(steamapps_dir+"/compatdata/107410/pfx/user.reg", 'r+') as nf:
                            insertPoint = "\""+insertPoint+"\"=\"native,builtin\""
                            dllOverride = "\""+dllName+"\"=\""+loadType+"\""
                            text = re.sub(insertPoint, insertPoint+'\n'+dllOverride, text)
                            nf.seek(0)
                            nf.truncate()
                            nf.write(text)
                            print ("Trying to overwrite "+dllName+"...")
                            os.system("sleep 1")

        override_dll("imagehlp", "native,builtin", "concrt140")
        override_dll("api-ms-win-crt-private-l1-1-0", "native,builtin", "api-ms-win-crt-math-l1-1-0")

    def steam_cmd(base_dir, steamapps_dir, app_name, app_id, force_windows):
        if not os.path.isdir(base_dir+"/Resources/steamcmd"):
            print ("Downloading steamcmd...")
            os.mkdir(base_dir+"/Resources/steamcmd")
            os.system('wget "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz" && tar -xf steamcmd_linux.tar.gz -C '+base_dir+'/Resources/steamcmd')

        steam_user = input("Please enter your Steam username: ")
        steamcmd = base_dir+"/Resources/steamcmd/steamcmd.sh"
        login = "+login "+steam_user
        install_dir = str('+force_install_dir "'+steamapps_dir+"/common"+'/'+app_name+'"')
        app_update = "+app_update "+app_id
        if force_windows == True:
            platform_type = "+@sSteamCmdForcePlatformType windows"
        else:
            platform_type = "+@sSteamCmdForcePlatformType linux"
        subprocess.call([steamcmd, platform_type, login, install_dir, app_update, "validate", "+quit"])
        return

    def install_proton(base_dir, steamapps_dir):
        print ("Installing Proton...")
        steam_cmd(base_dir, steamapps_dir, "Proton 4.2", "1054830", False)
        print ("Extracting dist...")
        tar = tarfile.open(steamapps_dir+"/common/Proton 4.2/proton_dist.tar.gz", mode="r:gz")
        tar.extractall(path=steamapps_dir+"/common/Proton 4.2/dist")
        tar.close()
        os.system("cp "+steamapps_dir+"/common/Proton\\ 4.2/version "+steamapps_dir+"/common/Proton\\ 4.2/dist/")

    # MAIN
     
    steamapps_dir, steam_library = find_libraries()
    base_dir = os.getcwd()

    print ("Finding Steam library locations...")
    find_libraries()
    print ("Detecting OS and distro...")
    for distro in ["Debain", "Ubuntu", "PopOS", "Fedora", "Arch"]:
        if detect(distro):
            install(distro, base_dir, steamapps_dir)
            break
    else:
        print (platform.release()+" Is not yet supported")

if __name__ == '__main__':
    main()

# vim: set syntax=python:
